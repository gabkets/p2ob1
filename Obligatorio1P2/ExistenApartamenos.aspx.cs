﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Obligatorio1P2Dominio;

namespace Obligatorio1P2
{
    public partial class ExistenApartamenos : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btn_validarMetros_Click(object sender, EventArgs e)
        {
            if (txtMetrosMin.Text!= "" && txtMetrosMax.Text!= "")
            {
                lblConfirmacion.Text = Sistema.Instancia.ValidarApto(txtMetrosMin.Text, txtMetrosMax.Text);
            }
            else
            {
                lblConfirmacion.Text = "Los campos no se pueden encontrar vacios";
            }
        }
    }
}