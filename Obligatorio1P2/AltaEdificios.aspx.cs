﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Obligatorio1P2Dominio;

namespace Obligatorio1P2
{
    public partial class AltaEdificios : System.Web.UI.Page
    {
        public string[] txtOrientaciones = { "N", "NE", "E", "SE", "S", "SO", "O", "NO" };
        public List<string> oritentaciones = new List<string>();

        protected void Page_Load(object sender, EventArgs e)
        {
           
            if (!IsPostBack) {
                foreach (string orientacion in txtOrientaciones)
                {
                    // Populamos la lista orientacion desde txtOrientacion
                    oritentaciones.Add(orientacion);
                }

                // Populamos la ddlOrientacion con la lista de orientaciones.
                ddlOrientacion.DataSource = oritentaciones;
                ddlOrientacion.DataBind();
                TipoOficina.Visible = false;
                TipoCasaHabitacion.Visible = false;   
            }

            linkMensaje.Visible = false;
        }

        protected void cambiarVisibilidadOficina(object sender, EventArgs e)
        {
            TipoOficina.Visible = true;
            TipoCasaHabitacion.Visible = false;
        }

        protected void cambiarVisibilidadCasaHabitacion(object sender, EventArgs e)
        {
            TipoOficina.Visible = false;
            TipoCasaHabitacion.Visible = true;
        }

        protected void btn_ClickEdificio_Click(object sender, EventArgs e)
        {

            int intPiso;
            int intNumero;
            int intMetraje;
            double intPrecio;
            int intPuestos;
            int intDormitorios;
            int intBanos;
            int.TryParse(txtPiso.Text, out intPiso);
            int.TryParse(txtNumero.Text, out intNumero);
            int.TryParse(txtMetraje.Text, out intMetraje);
            double.TryParse(txtPrecio.Text, out intPrecio);
            int.TryParse(txtPuestos.Text, out intPuestos);
            int.TryParse(txtDormitorios.Text, out intDormitorios);
            int.TryParse(txtBanos.Text, out intBanos);

            if (txtNomEdificio.Text != "" && txtDirEdificio.Text != "" && txtPiso.Text !="" && txtNumero.Text !="" && txtMetraje.Text != "" && txtPrecio.Text != "")
            {
                lblMensaje.Text = Sistema.Instancia.AltaEdificio(txtNomEdificio.Text, txtDirEdificio.Text);
                linkMensaje.Visible = true;
                if (oficina.Checked
                    && txtPuestos.Text != null
                    && lblMensaje.Text != "Ya existe un Edificio con el mismo nombre")
                {
                    lblMensaje.Text = Sistema.Instancia.altaOficina(intPiso, intNumero, txtNomEdificio.Text, intMetraje, intPrecio, ddlOrientacion.SelectedValue, intPuestos, boolEqupamiento.Checked);
                }

                if (casa_habitacion.Checked
                    && txtDormitorios.Text != null
                    && txtBanos.Text != null
                    && lblMensaje.Text != "Ya existe un Edificio con el mismo nombre")
                {
                    lblMensaje.Text = Sistema.Instancia.altaCasaHabitacion(intPiso, intNumero, txtNomEdificio.Text, intMetraje, intPrecio, ddlOrientacion.SelectedValue, intDormitorios, intBanos, boolGaraje.Checked);
                }
            }
            else
            {
                lblMensaje.Text = "No puede haber campos vacios";
            }
        }
    }
}