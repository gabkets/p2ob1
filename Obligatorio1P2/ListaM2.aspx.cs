﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Obligatorio1P2Dominio;

namespace Obligatorio1P2
{
    public partial class ListaM2 : System.Web.UI.Page
    {
        public string[] txtOrientaciones = { "N", "NE", "E", "SE", "S", "SO", "O", "NO" };
        public List<string> oritentaciones = new List<string>();
        public List<Apartamento> apartamentos = new List<Apartamento>();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                foreach (string orientacion in txtOrientaciones)
                {
                    // Populamos la lista orientacion desde txtOrientacion
                    oritentaciones.Add(orientacion);


                    // Populamos la ddlOrientacion con la lista de orientaciones.
                    ddlOrientacion.DataSource = oritentaciones;
                    ddlOrientacion.DataBind();
                }
            }
        }
        protected void btn_FiltrarMetros_Click(object sender, EventArgs e)
        {
            if (txtM2Min.Text != "" && txtM2Max.Text != "")
            {
                List<string> listTexto = new List<string>();

                foreach (Apartamento apartamento in Sistema.Instancia.BuscarMetros(txtM2Min.Text, txtM2Max.Text, ddlOrientacion.Text)) {
                    string texto = "<div class='resultado'>Metraje: " + apartamento.Metraje + " - Edificio: " + apartamento.Edificio.Nombre + " - Apartamento: " + apartamento.Numero + " - Piso: " + apartamento.Piso + "</div>";
                    listTexto.Add(texto);
                };


                if (listTexto.Count > 0)
                {
                    lblResultado.Text = string.Join("", listTexto);
                }
                else
                {
                    lblResultado.Text = "No hay edificios en ese rango";
                }

            }
            else
            {
                lblResultado.Text = "Campo minima cantidad y/o cantidad maxima de m2 no se pueden encontrar vacios";
            }
        }
    }
}