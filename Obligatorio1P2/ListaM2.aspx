﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ListaM2.aspx.cs" Inherits="Obligatorio1P2.ListaM2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Apartamentos por Metraje</title>
    <link href="~/css/main.css" rel="stylesheet" type="text/css" media="screen" runat="server" />  
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <nav class="navegacion">
            <ul>
                <li class="nav-item">
                    <a href="AltaEdificios.aspx" class="nav-link">Alta Edificios</a>
                </li>
                <li class="nav-item">
                    <a href="AltaApartamento.aspx" class="nav-link">Alta Apartamentos</a>
                </li>
                <li class="nav-item">
                    <a href ="ListaPrecio.aspx" class="nav-link activo">Listado por Precio</a>
                </li>
                    <li class="nav-item">
                    <a href ="ListaM2.aspx" class="nav-link ">Listado por m2</a>
                </li>
                    <li class="nav-item">
                    <a href ="ExistenApartamenos.aspx" class="nav-link ">Existen Edificios</a>
                </li>
            </ul>
        </nav>
    
        <asp:Panel ID="listaM2" CssClass="contenedor" runat="server">
            <h1>Listar por rango de Metros</h1>
            <section class="form-item">
                <asp:Label ID="lblCantidadM2" runat="server" Text="Mínimo: "></asp:Label>
                <asp:TextBox ID="txtM2Min" runat="server"></asp:TextBox>
            </section>
            <section class="form-item">
                <asp:Label ID="Label1" runat="server" Text="Máximo: "></asp:Label>
                <asp:TextBox ID="txtM2Max" runat="server"></asp:TextBox>
            </section>
            <section class="form-item">
                <asp:Label ID="Label2" runat="server" Text="Orientación: "></asp:Label>
                <asp:DropDownList ID="ddlOrientacion" runat="server" ></asp:DropDownList>
            </section>
            <section class="form-item">
                <asp:Button ID="btn_FiltrarMetros" runat="server" OnClick="btn_FiltrarMetros_Click" Text="Buscar Edificio" />
            </section>
            <asp:Label ID="lblResultado" runat="server"></asp:Label>
        </asp:Panel>
    
    </div>
    </form>
</body>
</html>
