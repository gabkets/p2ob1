﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ExistenApartamenos.aspx.cs" Inherits="Obligatorio1P2.ExistenApartamenos" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Edificios por Metraje</title>
    <link href="~/css/main.css" rel="stylesheet" type="text/css" media="screen" runat="server" />  
</head>
<body>
    <form id="form1" runat="server">
        <div>
           <nav class="navegacion">
                <ul>
                    <li class="nav-item">
                        <a href="AltaEdificios.aspx" class="nav-link">Alta Edificios</a>
                    </li>
                    <li class="nav-item">
                        <a href="AltaApartamento.aspx" class="nav-link">Alta Apartamentos</a>
                    </li>
                    <li class="nav-item">
                        <a href ="ListaPrecio.aspx" class="nav-link activo">Listado por Precio</a>
                    </li>
                       <li class="nav-item">
                        <a href ="ListaM2.aspx" class="nav-link ">Listado por m2</a>
                    </li>
                     <li class="nav-item">
                        <a href ="ExistenApartamenos.aspx" class="nav-link ">Existe Edificio</a>
                    </li>
                </ul>
            </nav>
            <asp:Panel ID="listarEdificios" CssClass="contenedor" runat="server">
                <h1>Listar edificios</h1>
                <asp:Label ID="lvlMetraje" runat="server" Text="Indicar rango de metraje: "></asp:Label>
                <section class="form-item">
                    <asp:Label ID="lblCantidadM2" runat="server" Text="Mínimo: "></asp:Label>
                    <asp:TextBox ID="txtMetrosMin" runat="server"></asp:TextBox>
                </section>
                <section class="form-item">
                    <asp:Label ID="lblCantidadM2Maximo" runat="server" Text="Máximo: "></asp:Label>
                    <asp:TextBox ID="txtMetrosMax" runat="server"></asp:TextBox>
                </section>
                <section class="form-item">
                    <asp:Button ID="btn_validarMetros" runat="server" OnClick="btn_validarMetros_Click" Text="Validar Edificios" />
                </section>
                <asp:Label ID="lblConfirmacion" runat="server"></asp:Label>
            </asp:Panel>
        </div>
    </form>
</body>
</html>
