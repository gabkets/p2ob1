﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ListaPrecio.aspx.cs" Inherits="Obligatorio1P2.ListaPrecio" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Apartamentos por Precio</title>
    <link href="~/css/main.css" rel="stylesheet" type="text/css" media="screen" runat="server" />  
</head>
<body>
    <form id="form1" runat="server">
    <div>
         <nav class="navegacion">
                <ul>
                    <li class="nav-item">
                        <a href="AltaEdificios.aspx" class="nav-link">Alta Edificios</a>
                    </li>
                    <li class="nav-item">
                        <a href="AltaApartamento.aspx" class="nav-link">Alta Apartamentos</a>
                    </li>
                    <li class="nav-item">
                        <a href ="ListaPrecio.aspx" class="nav-link activo">Listado por Precio</a>
                    </li>
                       <li class="nav-item">
                        <a href ="ListaM2.aspx" class="nav-link ">Listado por m2</a>
                    </li>
                     <li class="nav-item">
                        <a href ="ExistenApartamenos.aspx" class="nav-link ">Existen Edificios</a>
                    </li>
                </ul>
            </nav>
        <asp:Panel ID="listaPrecio" CssClass="contenedor" runat="server">
            <h1>Listar por rango de precios</h1>
            <section class="form-item">
                <asp:Label ID="lblListaPrecio" runat="server" Text="Mínimo:"></asp:Label>
                <asp:TextBox ID="txtPrecioMin" runat="server"></asp:TextBox>
            </section>
            <section class="form-item">
                <asp:Label ID="lblListaPrecioMaximo" runat="server" Text="Máximo:"></asp:Label>
                <asp:TextBox ID="txtPrecioMax" runat="server"></asp:TextBox>
                
            </section>
            <section class="form-item">
                <asp:Button ID="btn_FiltrarPrecio" runat="server" OnClick="btn_FiltrarPrecio_Click" Text="Buscar Apartamento" />
            </section>

            <asp:Label ID="lblResultado" runat="server"></asp:Label>
        </asp:Panel>
    
    </div>
    </form>
</body>
</html>
