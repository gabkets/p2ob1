﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Obligatorio1P2Dominio;

namespace Obligatorio1P2
{
    public partial class ListaPrecio : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btn_FiltrarPrecio_Click(object sender, EventArgs e)
        {
            if (txtPrecioMin.Text != "" && txtPrecioMax.Text != "")
            {
                List<string> listTexto = new List<string>();  

                foreach(Apartamento apartamento in Sistema.Instancia.BuscarPrecio(txtPrecioMin.Text, txtPrecioMax.Text)){
                    string texto = "<div class='resultado'>Precio: " + apartamento.precioTotal()  + " - Edificio: " + apartamento.Edificio.Nombre + " - Apartamento: " + apartamento.Numero + " - Piso " + apartamento.Piso + "</div>";
                    listTexto.Add(texto);
                };

                if (listTexto.Count > 0)
                {
                    lblResultado.Text = string.Join("", listTexto);
                }
                else {
                    lblResultado.Text = "No hay edificios en ese rango de precios";
                }
                
            }
            else
            {
                lblResultado.Text = "Campo Precio Minimo y/o Precio Maximo no se pueden encontrar vacios";
            }
        }
    }
}