﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AltaApartamento.aspx.cs" Inherits="Obligatorio1P2.AltaApartamento" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Alta Apartamentos</title>
    <link href="~/css/main.css" rel="stylesheet" type="text/css" media="screen" runat="server" />  
</head>
<body>
    <form id="form1" runat="server">
        <article>
            <nav class="navegacion">
                <ul>
                    <li class="nav-item">
                        <a href="AltaEdificios.aspx" class="nav-link">Alta Edificios</a>
                    </li>
                    <li class="nav-item">
                        <a href="AltaApartamento.aspx" class="nav-link">Alta Apartamentos</a>
                    </li>
                    <li class="nav-item">
                        <a href ="ListaPrecio.aspx" class="nav-link activo">Listado por Precio</a>
                    </li>
                       <li class="nav-item">
                        <a href ="ListaM2.aspx" class="nav-link ">Listado por m2</a>
                    </li>
                     <li class="nav-item">
                        <a href ="ExistenApartamenos.aspx" class="nav-link ">Existen Edificios</a>
                    </li>
                </ul>
            </nav>
            <asp:Panel ID="apartamentoAlta" CssClass="contenedor" runat="server">
                <h1>Alta de Apartamentos</h1>
                <section class="form-item">
                    <asp:Label ID="lblPiso" runat="server" Text="Piso" AssociatedControlID="txtPiso"></asp:Label>
                    <asp:TextBox ID="txtPiso" runat="server" ></asp:TextBox>
                </section>
                <section class="form-item">
                    <asp:Label ID="lblNumero" runat="server" Text="Número" AssociatedControlID="txtNumero"></asp:Label>
                    <asp:TextBox ID="txtNumero" runat="server"></asp:TextBox>
                </section>
                <section class="form-item">
                    <asp:Label ID="lblEdificio" runat="server" Text="Edificio" AssociatedControlID="ddlEdificio"></asp:Label>
                    <asp:DropDownList ID="ddlEdificio" runat="server"></asp:DropDownList>
                </section>
                <section class="form-item">
                    <asp:Label ID="lblMetraje" runat="server" Text="Metraje(m2)" AssociatedControlID="txtMetraje"></asp:Label>
                    <asp:TextBox ID="txtMetraje" runat="server"></asp:TextBox>
                </section>
                <section class="form-item">
                    <asp:Label ID="lblPrecio" runat="server" Text="Precio Base por m2" AssociatedControlID="txtPrecio"></asp:Label>
                    <asp:TextBox ID="txtPrecio" runat="server"></asp:TextBox>
                </section>
                <section class="form-item">
                    <asp:Label ID="lblOrientacion" runat="server" Text="Orientación" AssociatedControlID="ddlOrientacion"></asp:Label>
                    <asp:DropDownList ID="ddlOrientacion" runat="server"></asp:DropDownList>
                </section>
                <section class="form-item">
                    <ul class="checkbox-list">
                        <li class="checkbox-list-item">
                            <asp:RadioButton ID="casa_habitacion" GroupName="tipoApartamento" AutoPostBack="True"  runat="server" OnCheckedChanged="cambiarVisibilidadCasaHabitacion" />
                            <asp:Label ID="lblTipoApartamento" runat="server" Text="Tipo Casa-Habitación" AssociatedControlID="casa_habitacion"></asp:Label>
                        </li>
                        <li class="checkbox-list-item">
                            <asp:RadioButton ID="oficina" GroupName="tipoApartamento" AutoPostBack="True"  runat="server" OnCheckedChanged="cambiarVisibilidadOficina"/>
                            <asp:Label ID="lblTipoOficina" runat="server" Text="Tipo Oficina" AssociatedControlID="oficina"></asp:Label>
                        </li>
                    </ul>                  
                </section>

                <asp:Panel ID="TipoOficina" runat="server">
                    <section class="form-item">
                        <asp:Label ID="lblPuestos" runat="server" Text="Puestos" AssociatedControlID="txtPuestos"></asp:Label>
                        <asp:TextBox ID="txtPuestos" runat="server"></asp:TextBox>
                    </section>
                    <section class="form-item">
                        <asp:Label ID="lblEqupamiento" runat="server" Text="Equipamiento" AssociatedControlID="boolEqupamiento"></asp:Label>
                        <asp:CheckBox ID="boolEqupamiento" runat="server" />
                    </section>
                </asp:Panel>
                <asp:Panel ID="TipoCasaHabitacion" runat="server">
                    <section class="form-item">
                        <asp:Label ID="lblDormitorios" runat="server" Text="Dormitorios" AssociatedControlID="txtDormitorios"></asp:Label>
                        <asp:TextBox ID="txtDormitorios" runat="server"></asp:TextBox>
                    </section>
                    <section class="form-item">
                        <asp:Label ID="lblBanos" runat="server" Text="Baños" AssociatedControlID="txtBanos"></asp:Label>
                        <asp:TextBox ID="txtBanos" runat="server"></asp:TextBox>
                    </section>
                    <section class="form-item">
                        <asp:Label ID="lblGaraje" runat="server" Text="Garaje" AssociatedControlID="boolGaraje"></asp:Label>
                        <asp:CheckBox ID="boolGaraje" runat="server" />
                    </section>
                </asp:Panel>
            
                <section class="form-item">
                    <asp:Button ID="altaApartamento" runat="server" Text="Ingresar" OnClick="altaApartamento_Click" />
                </section>

                <asp:Label ID="lblMensaje" runat="server"></asp:Label>
            </asp:Panel>
        </article>
    </form>
</body>
</html>
