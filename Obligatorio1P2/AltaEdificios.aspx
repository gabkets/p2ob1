﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AltaEdificios.aspx.cs" Inherits="Obligatorio1P2.AltaEdificios" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Alta Edificios</title>
    <link href="~/css/main.css" rel="stylesheet" type="text/css" media="screen" runat="server" />  
</head>
<body>
    <form id="form1" runat="server">
        <article>
            <nav class="navegacion">
                <ul>
                    <li class="nav-item">
                        <a href="AltaEdificios.aspx" class="nav-link">Alta Edificios</a>
                    </li>
                    <li class="nav-item">
                        <a href="AltaApartamento.aspx" class="nav-link">Alta Apartamentos</a>
                    </li>
                    <li class="nav-item">
                        <a href ="ListaPrecio.aspx" class="nav-link activo">Listado por Precio</a>
                    </li>
                       <li class="nav-item">
                        <a href ="ListaM2.aspx" class="nav-link ">Listado por m2</a>
                    </li>
                     <li class="nav-item">
                        <a href ="ExistenApartamenos.aspx" class="nav-link ">Existen Edificios</a>
                    </li>
                </ul>
            </nav>
            <asp:Panel ID="edificioAlta" CssClass="contenedor" runat="server">
                <h1>Alta de Edificios</h1>
                <section class="form-item">
                    <asp:Label ID="lblNomEdificio" runat="server" Text="Ingresar Nombre: "></asp:Label>
                    <asp:TextBox ID="txtNomEdificio" CssClass="form-item-big" runat="server"></asp:TextBox>    
                </section>
                
                <section class="form-item">
                    <asp:Label ID="lblDirEdificio" runat="server" Text="Ingresar Dirección: "></asp:Label>
                    <asp:TextBox ID="txtDirEdificio" CssClass="form-item-big" runat="server"></asp:TextBox>
                </section>
                <section class="form-item">
                    <asp:Label ID="lblPiso" runat="server" Text="Piso" AssociatedControlID="txtPiso"></asp:Label>
                    <asp:TextBox ID="txtPiso" runat="server" ></asp:TextBox>
                </section>
                <section class="form-item">
                    <asp:Label ID="lblNumero" runat="server" Text="Número" AssociatedControlID="txtNumero"></asp:Label>
                    <asp:TextBox ID="txtNumero" runat="server"></asp:TextBox>
                </section>
                <section class="form-item">
                    <asp:Label ID="lblMetraje" runat="server" Text="Metraje(m2)" AssociatedControlID="txtMetraje"></asp:Label>
                    <asp:TextBox ID="txtMetraje" runat="server"></asp:TextBox>
                </section>
                <section class="form-item">
                    <asp:Label ID="lblPrecio" runat="server" Text="Precio Base por m2" AssociatedControlID="txtPrecio"></asp:Label>
                    <asp:TextBox ID="txtPrecio" runat="server"></asp:TextBox>
                </section>
                <section class="form-item">
                    <asp:Label ID="lblOrientacion" runat="server" Text="Orientación" AssociatedControlID="ddlOrientacion"></asp:Label>
                    <asp:DropDownList ID="ddlOrientacion" runat="server"></asp:DropDownList>
                </section>
                <section class="form-item">
                    <ul class="checkbox-list">
                        <li class="checkbox-list-item">
                            <asp:RadioButton ID="casa_habitacion" GroupName="tipoApartamento" AutoPostBack="True"  runat="server" OnCheckedChanged="cambiarVisibilidadCasaHabitacion" />
                            <asp:Label ID="lblTipoApartamento" runat="server" Text="Tipo Casa-Habitacion" AssociatedControlID="casa_habitacion"></asp:Label>
                        </li>
                        <li class="checkbox-list-item">
                            <asp:RadioButton ID="oficina" GroupName="tipoApartamento" AutoPostBack="True"  runat="server" OnCheckedChanged="cambiarVisibilidadOficina"/>
                            <asp:Label ID="lblTipoOficina" runat="server" Text="Tipo Oficina" AssociatedControlID="oficina"></asp:Label>
                        </li>
                    </ul>                  
                </section>

                <asp:Panel ID="TipoOficina" runat="server">
                    <section class="form-item">
                        <asp:Label ID="lblPuestos" runat="server" Text="Puestos" AssociatedControlID="txtPuestos"></asp:Label>
                        <asp:TextBox ID="txtPuestos" runat="server"></asp:TextBox>
                    </section>
                    <section class="form-item">
                        <asp:Label ID="lblEqupamiento" runat="server" Text="Equipamiento" AssociatedControlID="boolEqupamiento"></asp:Label>
                        <asp:CheckBox ID="boolEqupamiento" runat="server" />
                    </section>
                </asp:Panel>
                <asp:Panel ID="TipoCasaHabitacion" runat="server">
                    <section class="form-item">
                        <asp:Label ID="lblDormitorios" runat="server" Text="Dormitorios" AssociatedControlID="txtDormitorios"></asp:Label>
                        <asp:TextBox ID="txtDormitorios" runat="server"></asp:TextBox>
                    </section>
                    <section class="form-item">
                        <asp:Label ID="lblBanos" runat="server" Text="Baños" AssociatedControlID="txtBanos"></asp:Label>
                        <asp:TextBox ID="txtBanos" runat="server"></asp:TextBox>
                    </section>
                    <section class="form-item">
                        <asp:Label ID="lblGaraje" runat="server" Text="Garaje" AssociatedControlID="boolGaraje"></asp:Label>
                        <asp:CheckBox ID="boolGaraje" runat="server" />
                    </section>
                </asp:Panel>
                
                <asp:Panel runat="server" ID="Envio">
                    <asp:Button ID="btn_ClickEdificio" runat="server" Text="Agregar Edificio" OnClick="btn_ClickEdificio_Click" />
                    <asp:Label ID="lblMensaje" runat="server"></asp:Label>
                </asp:Panel>

                <asp:Panel runat="server" ID="linkMensaje">
                     <a href="AltaApartamento.aspx" class="form-link">Desea dar de alta mas Apartamentos</a>
                </asp:Panel>
            </asp:Panel> 
        </article>
    </form>
</body>
</html>
