﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obligatorio1P2Dominio
{
    public class Oficina:Apartamento
    {
        #region Atributos
        private int puestos;
        private bool equipamiento;
        #endregion

        #region Metodos
        // Método constructor que crea un Apartamento de tipo CasaHabitacion con atributos de ambas clases.
        public Oficina(int piso, int numero, Edificio edificio, int metraje, string orientacion, double precio, bool equipamiento, int puestos)
            :base(piso, numero, edificio, metraje, orientacion, precio){
            this.equipamiento = equipamiento;
            this.puestos = puestos;
        }

        // se sobreescribe el calculo de la clase padre con los datos de la subclase
        public override double precioTotal()
        {
            double montoFijo = 100;
            double adicional = 1.1;
            double precioTotal = (base.Precio * base.Metraje) + (montoFijo * puestos); // se toma el precio y metraje del padre y se le agrega el monto fijo y puestos

            if (equipamiento) // si el atributo bool = true, se agrega adicional
            {
                precioTotal = precioTotal * adicional;
            }

            return precioTotal; // se retorna el precio calculado
        }


        #endregion
    }
}
