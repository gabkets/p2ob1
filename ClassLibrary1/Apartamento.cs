﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obligatorio1P2Dominio
{
    public abstract class Apartamento
    {
        #region Atributos
        private int piso;
        private int numero;
        private int metraje;
        private double precio;
        private string orientacion;
        private Edificio edificio;
        #endregion

        #region Popiedades
        // Ponemos estás propiedades para que sean consumidas sin alterar los permisos del atributo.
        public int Piso
        {
            get
            {
                return piso;
            }
        }

        public int Numero
        {
            get
            {
                return numero;
            }
        }

        public Edificio Edificio
        {
            get
            {
                return edificio;
            }
        }

        public int Metraje
        {
            get
            {
                return metraje;
            }
        }

        public double Precio
        {
            get
            {
                return precio;
            }
        }

        public string Orientacion
        {
            get
            {
                return orientacion;
            }
        }
        #endregion

        #region Metodos
        // Método consutrctor de la clase
        public Apartamento(int piso, int numero, Edificio edificio, int metraje, string orientacion, double precio)
        {
            this.piso = piso;
            this.numero = numero;
            this.edificio = edificio;
            this.metraje = metraje;
            this.orientacion = orientacion;
            this.precio = precio;
        }

        public abstract double precioTotal(); //se declara abstracto para que cada subclase calcule el valor con sus atributos especificos
        #endregion
    }
}
