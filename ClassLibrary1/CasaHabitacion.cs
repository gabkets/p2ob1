﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obligatorio1P2Dominio
{
    public class CasaHabitacion : Apartamento 
    {
        #region Atributos
        private int dormitorios;
        private int banos;
        bool garaje;
        #endregion

        #region Metodos
        // Método constructor que crea un Apartamento de tipo CasaHabitacion con atributos de ambas clases.
        public CasaHabitacion(int piso, int numero, Edificio edificio, int metraje, string orientacion, double precio, int dormitorios, int banos, bool garaje)
            : base(piso, numero, edificio, metraje, orientacion, precio) { 
            this.dormitorios = dormitorios;
            this.banos = banos;
            this.garaje = garaje;
        }

        // Se sobreescribe el calculo de la clase padre con los datos de la subclase
        public override double precioTotal() 
        {
            double adicional = 1.2;
            double costoGaraje = 100;
            double costoOrientacion = 1.15;

            if (dormitorios < 3)
            {
                adicional = 1.05;
            }
            else if (2 < dormitorios && dormitorios < 5)
            {
                adicional = 1.1;
            }

            double precioTotal = (Precio * Metraje) * adicional;

            if (garaje)
            {
                precioTotal = precioTotal + costoGaraje;
            }

            if (Orientacion == "N" || Orientacion == "NE" || Orientacion == "NO")
            {
                precioTotal = precioTotal * costoOrientacion;
            }

            return precioTotal;
        }
        #endregion
    }
}
