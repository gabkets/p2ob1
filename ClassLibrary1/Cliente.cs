﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obligatorio1P2Dominio
{
    public class Cliente
    {
        #region Atributos
        private string nombre;
        private string apellido;
        private string documento;
        private string direccion;
        private int telefono;
        private List<CompraRealizada0> compra;
        #endregion

        #region Metodos
        public Cliente(string nombre, string apellido, string documento, string direccion, int telefono)
        {
            this.nombre = nombre;
            this.apellido = apellido;
            this.documento = documento;
            this.direccion = direccion;
            this.telefono = telefono;
        }
        #endregion

    }
}
