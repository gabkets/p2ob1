﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obligatorio1P2Dominio
{
    public class Sistema
    {
        private List<Edificio> edificios = new List<Edificio>(); // se crean las listas de Edificio y Apartamento en la clase controladora
        private List<Apartamento> apartamentos = new List<Apartamento>();
        private static Sistema instancia;

        #region Propiedades
        // Ponemos éstas propiedades para que sean consumidas sin alterar los permisos del atributo.
        public static Sistema Instancia 
        {
            get
            {
                if (instancia == null)
                {
                    instancia = new Sistema();
                }
                return instancia;
            }
        }

        public List<Edificio> Edificios {
            get
            {
                return edificios;
            }
        }

        private Sistema() // singleton de Sistema
        {

        }
        #endregion

        #region Apartamentos
        // Metodo para dar de alta una oficina
        public string altaOficina(int piso, int numero, string edificio, int metraje, double precio, string orientacion, int puestos, bool equipamiento)
        {
            string mensaje = "";

            Edificio edificioAsociado = asociarEdificio(edificio); // se llama al metodo para asociar un edificio al apartamento

            if (this.apartamentoExistente(piso, numero, edificio) == null)// se controla que no exista el mismo apartamento
            {
                Oficina o = new Oficina(piso, numero, edificioAsociado, metraje, orientacion, precio, equipamiento, puestos);
                apartamentos.Add(o);// se agrega a la lista apartamentos el nuevo apartamento y se asocia.
                edificioAsociado.asociarApartamento(o);
                mensaje = "Apartamento en edificio " + edificio + " en el piso " + piso + " de numero " + numero + " fue guardado ";
            }
            else {
                mensaje = "Apartamento ya existe en ese edificio y en ese piso."; // de ya existir se envia mensaje de error
            }

            return mensaje;
        }

        //Metodo para dar de alta una Casa-Habitacion
        public string altaCasaHabitacion(int piso, int numero, string edificio, int metraje, double precio, string orientacion, int dormitorios, int banos, bool garaje)
        {
            Edificio edificioAsociado = asociarEdificio(edificio); //se llama al metodo para asociar un edificio al apartamento
            string mensaje = "";

            if (this.apartamentoExistente(piso, numero, edificio) == null) // se controla que no exista el mismo apartamento
            {
                CasaHabitacion ch = new CasaHabitacion(piso, numero, edificioAsociado, metraje, orientacion, precio, dormitorios, banos, garaje);
                apartamentos.Add(ch); // se agrega a la lista apartamentos el nuevo apartamento y se asocia.
                edificioAsociado.asociarApartamento(ch);
                mensaje = "Apartamento en edificio " + edificio + " en el piso " + piso + " de numero " + numero + " fue guardado ";
            }
            else
            {
                mensaje = "Apartamento ya existe en ese edificio y en ese piso."; // de ya existir se envia mensaje de error
            }

            return mensaje;
        }
        //Metodo de verificacion de existencia de apartamento
        public Apartamento apartamentoExistente(int piso, int numero, string edificio)
        {
            Apartamento a = null;
            int i = 0;

            while(i < apartamentos.Count && a == null){ // en caso de encontrar igualdad retorna sino recorre toda la lista.
                if (apartamentos[i].Numero == numero && apartamentos[i].Piso == piso && apartamentos[i].Edificio.Nombre == edificio) {
                    a = apartamentos[i];
                }
                i++;
            }

            return a;
        }
        #endregion

        #region Edificios
        //Metodo para dar de alta Edificio
        public string AltaEdificio(string nombre, string direccion) // recibe parametos string nombre y direccion
        {
            string mensaje = "";
            if (nombre != "" && direccion != "") // control de que no haya datos vacios.
            {
                Edificio e = BuscarEdificio(nombre, direccion);// se verifica que ya no exista un edificio con los mismos datos
                if (e == null)
                {
                    e = new Edificio(nombre, direccion);// de no existir, se crea nuevo edificio con los parametros asignados y se agrega a la lista edificios junto con un mensaje afirmativo
                    edificios.Add(e);
                    mensaje = "El Edificio se dío de alta correctamente";
                }
                else
                {
                    mensaje = "Ya existe un Edificio con el mismo nombre"; // de existir edificio en la lista edificios muestra mensaje error.
                }
            }
            return mensaje;
        }
        
        //Metodo para buscar Edificio
        public Edificio BuscarEdificio(string nombre, string direccion)
        {
            Edificio e = null;
            int i = 0;
            while (i < edificios.Count && e == null) // en caso de encontrar igualdad retorna sino recorre toda la lista.
            {
                if (edificios[i].Nombre == nombre && edificios[i].Direccion == direccion)
                {
                    e = edificios[i];
                }
                i++;
            }
            return e;
        }

        //Metodo para asociar edificio a apartamento
        public Edificio asociarEdificio(string nombre) { // se retorna objeto Edificio, pasandose como parametro nombre ya que es unico
            int i = 0;
            Edificio edificio = null;
            bool flag = true;

            while (i < edificios.Count && flag) { 
                if (edificios[i].Nombre == nombre) {
                    edificio = edificios[i]; 
                }
                i++;
            }
            
            return edificio; // al encontar igualdad retorna la posicion del objeto en la lista
        }
        #endregion

        #region Listado

        //Metodo para Buscar por precio
        public List<Apartamento> BuscarPrecio(string min, string max)//se pasa parametro maximo y minimo
        {
            List<Apartamento> lista = new List<Apartamento>();// se crea una nueva lista para guardar los apartamentos en dicho rango
            double precioMin = Convert.ToDouble(min);//se convieren los string a double
            double precioMax = Convert.ToDouble(max);

            for (int i=0; i < apartamentos.Count; i++ )
            {
                if(apartamentos[i].precioTotal()<precioMax && apartamentos[i].precioTotal()>precioMin)
                {
                    lista.Add(apartamentos[i]);//se recorre la lista aparamentos si el valor se encuentra en el rango se agregan a la nueva lista
                }
            }

            return lista;
        }

        //Metodo para buscar por m2
        public List<Apartamento> BuscarMetros(string min, string max, string orientacion)
        {
            List<Apartamento> lista = new List<Apartamento>(); // se crea nueva lista para guardar los apartamentos filtrados
            double m2Min = Convert.ToDouble(min); // se convierten los string a double
            double m2Max = Convert.ToDouble(max);
            for (int i = 0; i < apartamentos.Count; i++)
            {
                if (apartamentos[i].Metraje <= m2Max && apartamentos[i].Metraje >= m2Min && orientacion == apartamentos[i].Orientacion)
                {

                    lista.Add(apartamentos[i]);
                }
            }
            return lista; // se realiza un join para mostrar los string concatenados 
        }

        //Metodo para Validacion de Apartamento
        public string ValidarApto (string min,string max)
        {
            string mensaje = "";
            int m2Min;
            int m2Max;
            int.TryParse(min, out m2Min);
            int.TryParse(max, out m2Max);


            int i = 0;
            bool flag = true;
            while (i < apartamentos.Count && flag) { //se recorre con un while para que a la primera coincidencia se termine la busqueda

                if (apartamentos[i].Metraje <= m2Max && apartamentos[i].Metraje >= m2Min)
                {
                    mensaje = "Hay edificios con apartamentos en el rango de las medidas dadas. <a href='ListaM2.aspx'>Buscar apartamentos por métros</a>";
                }
                else
                {
                    mensaje = "No se encuentran edificios con apartamentos en el rango de las medidas dadas."; //de no encontrar se muestra mensaje error

                }
                i++;
            }
            
            return mensaje;
        }
    }
        #endregion
    }

