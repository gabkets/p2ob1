﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obligatorio1P2Dominio
{
    public class Edificio
    {
        #region Atributos
        private string nombre;
        private string direccion;
        private List<Apartamento> apartamentos = new List<Apartamento>();
        #endregion

        #region Propiedades
        // Ponemos estás propiedades para que sean consumidas sin alterar los permisos del atributo.
        public string Nombre
        {
            get
            {
                return nombre;
            }
        }

        public string Direccion
        {
            get
            {
                return direccion;
            }
        }

        public Edificio(string nombre, string direccion)
        {
            this.nombre = nombre;
            this.direccion = direccion;
        }
        #endregion

        // Usamos este método para asociar apartamento correspondientes al edificio creado.
        public void asociarApartamento(Apartamento apartamento) {

            apartamentos.Add(apartamento);
            
        }

    }
}
