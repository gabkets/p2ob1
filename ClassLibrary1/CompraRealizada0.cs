﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obligatorio1P2Dominio
{
    public class CompraRealizada0
    {
        #region Atributos
        private DateTime fecha;
        private List<string> nomVendedor;
        private double precio;
        private Cliente cliente;
        private List<Apartamento> apartamento;
        #endregion

        #region Metodos
        public CompraRealizada0(DateTime fecha, List<string> nomVendedor, double precio, Cliente cliente, List<Apartamento> apartamento)
        {
            this.fecha = fecha;
            this.nomVendedor = nomVendedor;
            this.precio = precio;
            this.cliente =cliente;
            this.apartamento = apartamento;
        }
        #endregion
    }
}
